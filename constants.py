"""Constants initialization

All fixed and constants are declared here for ease of use
"""

import numpy as np

DISLODGED_CONSTANT = 0.05
RON97_THRESH_LO = np.array([73, 73, 0])
RON97_THRESH_HI = np.array([86, 255, 255])
RON95_THRESH_LO = np.array([0, 69, 140])
RON95_THRESH_HI = np.array([76, 255, 255])
EU5_THRESH_LO = np.array([91, 89, 52])
EU5_THRESH_HI = np.array([110, 255, 205])
ADO_THRESH_LO = np.array([111, 0, 0])
ADO_THRESH_HI = np.array([135, 255, 98])

