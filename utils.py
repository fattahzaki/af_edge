import signal
import cv2
from skimage import exposure

def get_mask(img, lo, hi):
    """Get the HSV channel color mask from an image based on the threshold

    Parameters
    ----------
    img : numpy.Array
        Frame to get the mask from
    lo : numpy.Array
        Minimum HSV channel value
    hi : numpy.Array
        Maximum HSV channel value

    Returns
    -------
    numpy.Array
        The resulting mask value
    """

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    masked = cv2.inRange(hsv, lo, hi)
    return masked

def match_histo(src, ref):
    """Do histogram matching on an image

    Parameters
    ----------
    src : numpy.Array
        Source image to apply histogram matching to
    ref : numpy.Array
        Reference image to get the histogram

    Returns
    -------
        numpy.Array
            Return the image with the modified histogram
    """

    is_multi = True if src.shape[-1] > 1 else False
    matched = exposure.match_histograms(src, ref, multichannel=is_multi)
    return matched

class GracefulKiller:
    """
    A class to kill processes in a graceful manner. The process object is
    passed during construction. THe passed process objects must have kill()
    API to trigger graceful kill

    Methods
    -------
    exit_gracefully()
        Method to call kill() API for every process
    """

    kill_now = False

    def __init__(self, procs):
        """
        Parameters
        ----------
        procs : list
            List of objects to kill
        """

        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        self.procs = procs

    def exit_gracefully(self, *args):
        """Call kill API for every process"""

        self.kill_now = True
        for proc in self.procs:
            proc.kill()

