"""Autonomous Facility on Edge

A script to run various detection and analysis on edge for autonomous facility
purposes. Currently, the supported process is only breakaway-decoupling case
but eventually more processes will be supported
"""

import argparse
import json
import logging
import os
import shutil
import sys
import time
from datetime import datetime

from lib.pump_bay import PumpBay
from utils import GracefulKiller
from test.test import Test

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

def is_valid_json(parser, arg):
    """Sanity check for JSON file provided by the user"""

    if not os.path.exists(arg):
        parser.error(f"The file {arg} does not exist")
        return arg
    if not arg.endswith(".json"):
        parser.error(f"The file {arg} is not a JSON file")
        return arg

    # TODO: json schema check

    return arg

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config_file", required=True,
                        type=lambda x: is_valid_json(parser, x))
    parser.add_argument("--save_logs", default=False, action="store_true")
    parser.add_argument("--run_tests", default=False, action="store_true")
    parser.add_argument("--no_manual_roi", default=False, action="store_true")
    return parser.parse_args()

def init_pumps(data):
    """Initialize PumpBay object with relevant information

    Initialize each pump bay with relevant information and start media feed
    capture

    Parameters
    ----------
    data : dict
        Relevant pump data extracted from user-defined JSON file
    
    Returns
    -------
        list
            List of pump objects that have been initialized
    """

    pumps = []
    pumps_data = data["pump_bays"]
    logs_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            data["logs_dir"])
    if os.path.exists(logs_dir):
        shutil.rmtree(logs_dir)
    os.makedirs(logs_dir)

    for pump_data in pumps_data:
        pump = PumpBay(data["station_id"], pump_data, logs_dir)
        pump.start_cap(sample_rate=pump_data["cap_sample_rate"])
        pumps.append(pump)
    return pumps

if __name__ == "__main__":
    args = parse_args()
    save_logs = args.save_logs
    run_tests = args.run_tests
    if run_tests == True:
        save_logs = True
    no_manual_roi = args.no_manual_roi
    with open(args.config_file, "r") as f:
        config_dict = json.load(f)

    pumps = init_pumps(config_dict)
    if len(pumps) == 0:
        logger.error("No pumps data found")
        sys.exit(1)

    killer = GracefulKiller(pumps)

    while not killer.kill_now:
        for pump in pumps:
            ret = 1
            ret *= not pump.cap.is_running()
            if ret == True:
                break
            pump.run_detection("breakaway_decoupling",
                               is_manual_roi=(not no_manual_roi))
        if ret == True:
            break

    if save_logs == True:
        ts = datetime.now().strftime("%Y%m%d_%H:%M:%S")
        saved_logs_dir = os.path.abspath(f"saved_logs_{ts}")
        shutil.copytree(config_dict["logs_dir"], saved_logs_dir)
        logger.info(f"Detection result logs saved to {saved_logs_dir}")

    if run_tests == True:
        ret = 0
        logger.info("Running test now")
        test_obj = Test(os.path.join(saved_logs_dir, "logs.txt"), \
                        "test/ground_truth.json")
        ret += test_obj.run_test_breakaway_detection()
        if no_manual_roi == True:
            ret += test_obj.run_test_sign_detection()
        logger.info("Test done")
        sys.exit(ret)

