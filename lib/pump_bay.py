import logging
import os
import time
import cv2

from lib.cap import ImageCap, VideoCap
from lib.breakaway import RON97Breakaway, RON95Breakaway, EU5Breakaway, ADOBreakaway

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

class PumpBay():
    """
    This class represents one pump bay in every station. It holds information
    on Cap which represents the source of media feed and Breakaway, a class
    to represent each breakaway on each pump bay

    Methods
    -------
    start_cap(sample_rate=-1)
        Start capturing media feed
    stop_cap()
        Stop capturing media feed
    kill()
        An API used by GracefullKiller to run clean termination of processes
    run_detection(detection_type, frame=None)
        Trigger detection based on detection_type parameter
    """

    def __init__(self, station_id, data, logs_dir):
        """
        Parameters
        ----------
        station_id : str
            Name/location of pump station
        id : str
            Pump bay number
        dispenser_area : str
            Dispenser area/island number
        decoupled_logfile : str
            Path to file to log all breakaway-decoupling events
        breakaways : list
            List of all breakaways that exist on the pump bay
        """

        self.station_id = station_id
        self.id = data["pump_id"]
        self.dispenser_area = data["dispenser_area"]
        self.is_display = data["display"]
        self.is_report = data["report"]
        self.logfile = os.path.join(logs_dir, "logs.txt")
        self.decoupled_logfile = os.path.join(logs_dir, "decoupled_list.csv")

        if data["cap_type"] == "jpg":
            self.cap = ImageCap(data["cap_type"], data["cap_source"])
        elif data["cap_type"] == "video":
            self.cap = VideoCap(data["cap_type"], data["cap_source"])
        elif data["cap_type"] == "rtsp":
            self.cap = RTSPCap(data["cap_type"], data["cap_source"])
        else:
            raise NotSupportedError(f"{data['cap_type']} is not a supported " \
                                     "capture type")

        self.breakaways = []
        for key, val in data["breakaways"].items():
            if key == "ron97":
                breakaway = RON97Breakaway(key, val["roi"])
            elif key == "ron95":
                breakaway = RON95Breakaway(key, val["roi"])
            elif key == "eu5":
                breakaway = EU5Breakaway(key, val["roi"])
            elif key == "ado":
                breakaway = ADOBreakaway(key, val["roi"])
            else:
                continue
            self.breakaways.append(breakaway)

    def start_cap(self, sample_rate=-1):
        """Trigger start capture event to the media feed source

        Parameters
        ----------
        sample_rate : str, optional
            Period to sample media feed (default is -1 which is sampling all
            frames)
        """

        self.cap.start(sample_rate=sample_rate)

    def stop_cap(self):
        """Trigger stop capture event to the media feed source"""

        self.cap.stop()
        cv2.destroyAllWindows()

    def kill(self):
        """An API uses by GracefullKiller to run clean termination of processes
        """

        self.stop_cap()

    def run_detection(self, detection_type, frame=None, is_manual_roi=True):
        """Trigger detection based on detection_type

        If frame parameter is not specified, the method will get the newest
        frame from cap source and run detection. It will also display the frame
        after detection is being done

        Parameters
        ----------
        detection_type : str
            Type of detection to be run. Currently only supports breakaway
            decoupling
        frame : numpy.Array, optional
            Frame to run detection on (default is None)

        Returns
        -------
        bool
            Returns True if detection is being run, False if otherwise

        Raises
        ------
            NotSupportedError
                Raised if detection_type is not supported
        """

        if frame == None:
            ret, frame_obj = self.cap.get_newest_frame()
            if ret == False:
                return ret

        if detection_type == "breakaway_decoupling":
            img = frame_obj.frame
            if is_manual_roi == True:
                y = img.shape[0]
                cropped = img[0:y//2, :]
                for breakaway in self.breakaways:
                    if breakaway.detect_decoupling(cropped) == True and \
                            self.is_report == True:
                        self._report_decoupled(breakaway, frame_obj)
            else:
                ret, (x, y, w, h) = self._detect_sign(img)
                if ret == True:
                    cropped = img[y:y+h, x:x+w]
                    for breakaway in self.breakaways:
                        if breakaway.detect_decoupling2(cropped) == True and \
                                self.is_report == True:
                            self._report_decoupled(breakaway, frame_obj)
                else:
                    logger.warning("Can't detect landmark. The image might be " \
                                   "low quality")
                    with open(self.logfile, "a+") as f:
                        f.write(f"sign_not_detected,{frame_obj.pos}\n")
                    return False
            if self.is_display:
                cv2.imshow(self.id, img)
                cv2.waitKey(1)
        else:
            raise NotSupportedError(f"{detection_type} is not a supported " \
                                     "detection type")

        return True

    def _detect_sign(self, frame):
        """
        Detect Petronas sign on top of the pump to be utilized as a landmark
        """

        y = frame.shape[0]
        img = frame[0:y//2,:]
        img_mod = cv2.GaussianBlur(img, (9, 9), 0)
        img_mod = cv2.cvtColor(img_mod, cv2.COLOR_BGR2GRAY)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        img_mod = cv2.erode(img_mod, kernel, iterations=5)
        img_mod = cv2.dilate(img_mod, kernel, iterations=5)
        img_mod = cv2.Canny(img_mod, 30, 150)
        #canny = img_mod.copy()
        img_mod = cv2.dilate(img_mod, kernel, iterations=3)
        #dilated = img_mod.copy()

        contours, hierarchy = cv2.findContours(img_mod, cv2.RETR_EXTERNAL,
                                               cv2.CHAIN_APPROX_SIMPLE)

        # Filter out contours that are not closed and not rectangle-shaped
        # and too small
        filtered_conts = []
        for cnt in contours:
            area = cv2.contourArea(cnt)
            arc_len = cv2.arcLength(cnt, True)
            if area > arc_len and area >= 100000:
                approx = cv2.approxPolyDP(cnt, 0.08 * arc_len, True)
                _, _, w, h = cv2.boundingRect(approx)
                ratio = w/h
                if len(approx) == 4 and ratio >= 1.5:
                    filtered_conts.append(approx)

        # TODO: for testing purposes only
        #if len(filtered_conts) > 1:
        #    logger.debug(f"num of contour: {len(filtered_conts)}")
        #    for i, x in enumerate(filtered_conts):
        #        logger.debug(i)
        #        logger.debug(f"edge: {len(x)}")
        #        _, _, w, h = cv2.boundingRect(x)
        #        logger.debug(f"width: {w}")
        #        logger.debug(f"height: {h}")
        #        logger.debug(f"area: {cv2.contourArea(x)}")
        #    exit(1)
        #elif len(filtered_conts) == 0:
        #    cv2.imshow("canny", canny)
        #    cv2.imshow("dilated", dilated)
        #    cv2.waitKey(0)
        #    exit(1)

        # When no closed rectangle contour detected, the image is considered a
        # bad image
        if len(filtered_conts) == 0:
            return False, (-1, -1, -1, -1)

        # Sort by increasing order of contour area and the contour with
        # the largest area is what we want
        sorted_conts = sorted(filtered_conts, key=cv2.contourArea,
                             reverse=True)
        sign_cont = sorted_conts[0]
        #cv2.drawContours(img, [ sign_cont ], -1, (0, 0, 255), 5)
        x, y, w, h = cv2.boundingRect(sign_cont)
        yp = y + 3*h//2
        hp = h//2
        cv2.rectangle(frame, (x, yp), (x + w, yp + hp), (0, 0, 0), 5)
        return True, (x, yp, w, hp)

    def _report_decoupled(self, breakaway, frame_obj):
        """Reports breakaway-decoupling events

        This method is still WIP. Eventually, here is where we upload the
        breakaway-decoupling case to SAP. Right now, it displays the
        report to the CLI and write to the log file

        Parameters
        ----------
        breakaway : Breakaway
            Class that contains information on each breakaway
        frame_info : str, optional
            Extra frame information that needs to be reported (default is None)
        """

        # TODO: timestamp should be the time image is taken
        timestamp = time.time()
        report = f"{timestamp},{self.id},{self.dispenser_area}," \
                 f"{breakaway.id},{frame_obj.pos},{frame_obj.timestamp}\n"
        with open(self.decoupled_logfile, "a+") as f:
            f.write(report)
        logger.debug("Breakaway decoupling detected!")
        logger.debug(report)
        with open(self.logfile, "a+") as f:
            f.write(f"breakaway_decoupling,{breakaway.id},{frame_obj.pos}\n")

