import logging
import cv2
import numpy as np

from utils import get_mask
from constants import DISLODGED_CONSTANT
from constants import RON97_THRESH_LO, RON97_THRESH_HI
from constants import RON95_THRESH_LO, RON95_THRESH_HI
from constants import EU5_THRESH_LO, EU5_THRESH_HI
from constants import ADO_THRESH_LO, ADO_THRESH_HI

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

class Breakaway():
    """
    A parent class to represent one breakaway type on one pump bay, It can be
    RON97, RON95, EU5 or ADO breakaway

    Methods
    -------
    detect_decoupling(frame)
        Detect breakaway decoupling event
    """

    def __init__(self, breakaway_id, roi=None):
        """
        Parameters
        ----------
        id : str
            Type of breakaway
        roi : dict
            ROI for the breakaway in the frame
        """

        self.id = breakaway_id
        self.roi = roi

    def detect_decoupling(self, frame):
        """Detect breakaway decoupling event

        The decoupling detection is done by extracting the mask of the pixels
        that contains the breakaway based on the threshold values of its
        shades of color and use that to obtain a binary image of black and
        white where white pixels are the pixels of interest

        Parameters
        ----------
        frame : numpy.Array
            Frame to run detection on

        Returns
        -------
        bool
            Return True if decoupling is detected
        """

        img_ori = frame.copy()
        roi = self.roi
        img = img_ori[roi["y"]:roi["y"]+roi["h"],
                      roi["x"]:roi["x"]+roi["w"]]
        mask = get_mask(img, self.THRESH_LO, self.THRESH_HI)
        img_mod = cv2.bitwise_and(img, img, mask=mask)
        img_mod = cv2.cvtColor(img_mod, cv2.COLOR_BGR2GRAY)
        img_mod = cv2.threshold(img_mod, 1, 255, cv2.THRESH_BINARY)[1]
        ret = self._is_decoupled(img_mod)
        self._draw_bb(frame, decoupled=ret)
        return ret

    def detect_decoupling2(self, frame):
        img = cv2.GaussianBlur(frame, (21, 21), 0)
        mask = get_mask(frame, self.THRESH_LO, self.THRESH_HI)
        img_mod = cv2.bitwise_and(img, img, mask=mask)
        img_mod = cv2.cvtColor(img_mod, cv2.COLOR_BGR2GRAY)
        img_mod = cv2.threshold(img_mod, 1, 255, cv2.THRESH_BINARY)[1]
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        img_mod = cv2.erode(img_mod, kernel, iterations=5)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        img_mod = cv2.dilate(img_mod, kernel, iterations=5)
        color = img_mod.copy()

        img_mod = cv2.Canny(img_mod, 30, 150)
        canny = img_mod.copy()

        contours, hierarchy = cv2.findContours(img_mod, cv2.RETR_EXTERNAL,
                                               cv2.CHAIN_APPROX_SIMPLE)
        filtered_conts = []
        for cont in contours:
            area = cv2.contourArea(cont)
            arc_len = cv2.arcLength(cont, True)
            if area > arc_len and area >= 500 and area <= 2000:
                    approx = cv2.approxPolyDP(cont, 0.08 * arc_len, True)
                    _, _, w, h = cv2.boundingRect(approx)
                    ratio = h/w
                    if len(approx) == 4 and ratio > 1 and ratio < 2.5:
                        filtered_conts.append(approx)
        #cv2.drawContours(frame, filtered_conts, -1, (0, 0, 255), 5)

        if len(filtered_conts) == 0:
            return True

        sorted_conts = sorted(filtered_conts, key=cv2.contourArea, reverse=True)
        res_cont = sorted_conts[0]

        # TODO: for testing only
        #if len(filtered_conts) > 1:
        #    # TODO: get the one with the largest area
        #    logger.error("More than one contour detected!")
        #    logger.debug(f"# of contours: {len(filtered_conts)}")
        #    for i, x in enumerate(filtered_conts):
        #        logger.debug(f"Contour #: {i}")
        #        _, _, w, h = cv2.boundingRect(x)
        #        ratio = h/w
        #        area = cv2.contourArea(x)

        #        area_res = f"Area: {area}, FAIL" if area < 500 or area > 2000 \
        #                   else f"Area: {area}, PASS"
        #        edge_res = f"Edge: {len(x)}, FAIL" if len(x) != 4 else \
        #                   f"Edge: {len(x)}, PASS"
        #        ratio_res = f"Ratio: {ratio}, FAIL" if ratio <= 1 or \
        #                    ratio >=2.5 else f"Ratio: {ratio}, PASS"

        #        logger.debug(area_res)
        #        logger.debug(edge_res)
        #        logger.debug(ratio_res)
        #    cv2.drawContours(frame, filtered_conts, -1, (0, 0, 255), 5)
        #    cv2.imshow("more than one", frame)
        #    cv2.waitKey(0)
        #    exit(1)
        #elif len(filtered_conts) == 0:
        #    cv2.imshow("color", color)
        #    cv2.imshow("canny", canny)
        #    cv2.waitKey(0)
        #    exit(1)

        x, y, w, h = cv2.boundingRect(res_cont)
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 3)
        return False

    def _is_decoupled(self, img):
        """Calculate the ratio of white pixels to black pixels

        Parameters
        ----------
        img : numpy.Array
            Binary black and white image frame

        Returns
        -------
        bool
            Return True if ratio of white pixels to black pixels is less than
            DISLODGED_CONSTANT
        """

        num_white_pix = np.sum(img == 255)
        total_pix = img.shape[0] * img.shape[1]
        ratio = num_white_pix / total_pix
        #logger.debug("{} white pixel ratio: {:.2f}".format(self.id, ratio))
        return True if ratio <= DISLODGED_CONSTANT else False

    def _draw_bb(self, img, decoupled=False):
        """Draw bounding box of detected breakaway

        Parameters
        ----------
        img : numpy.Array
            Image frame to draw the bounding box on
        decoupled : bool, optional
            Indicates whether the detected breakaway is decoupled (default is
            False)
        """

        color = (0, 0, 255) if decoupled == True else (0, 255, 0)
        roi = self.roi
        cv2.rectangle(img, (roi["x"], roi["y"]),
                      (roi["x"] + roi["w"], roi["y"] + roi["h"]),
                      color, 3)

class RON97Breakaway(Breakaway):
    """Child class of Breakaway for RON97 breakaway type"""

    def __init__(self, breakaway_id, roi=None):
        """
        Parameters
        __________
        THRESH_LO : numpy.Array
            Minimum HSV channel value of the shades of color of the breakaway
        THRESH_HI : numpy.Array
            Maximum HSV channel value of the shades of color of the breakaway
        """

        Breakaway.__init__(self, breakaway_id, roi)
        self.THRESH_LO = RON97_THRESH_LO
        self.THRESH_HI = RON97_THRESH_HI

class RON95Breakaway(Breakaway):
    """Child class of Breakaway for RON95 breakaway type"""

    def __init__(self, breakaway_id, roi=None):
        """
        Parameters
        __________
        THRESH_LO : numpy.Array
            Minimum HSV channel value of the shades of color of the breakaway
        THRESH_HI : numpy.Array
            Maximum HSV channel value of the shades of color of the breakaway
        """

        Breakaway.__init__(self, breakaway_id, roi)
        self.THRESH_LO = RON95_THRESH_LO
        self.THRESH_HI = RON95_THRESH_HI

class EU5Breakaway(Breakaway):
    """Child class of Breakaway for EU5 breakaway type"""

    def __init__(self, breakaway_id, roi=None):
        """
        Parameters
        __________
        THRESH_LO : numpy.Array
            Minimum HSV channel value of the shades of color of the breakaway
        THRESH_HI : numpy.Array
            Maximum HSV channel value of the shades of color of the breakaway
        """

        Breakaway.__init__(self, breakaway_id, roi)
        self.THRESH_LO = EU5_THRESH_LO
        self.THRESH_HI = EU5_THRESH_HI

class ADOBreakaway(Breakaway):
    """Child class of Breakaway for ADO breakaway type"""

    def __init__(self, breakaway_id, roi=None):
        """
        Parameters
        __________
        THRESH_LO : numpy.Array
            Minimum HSV channel value of the shades of color of the breakaway
        THRESH_HI : numpy.Array
            Maximum HSV channel value of the shades of color of the breakaway
        """

        Breakaway.__init__(self, breakaway_id, roi)
        self.THRESH_LO = ADO_THRESH_LO
        self.THRESH_HI = ADO_THRESH_HI

    def detect_decoupling2(self, frame):
        w = frame.shape[1]
        img = frame[:, 0:w//4]
        img = cv2.GaussianBlur(img, (21, 21), 0)
        mask = get_mask(img, self.THRESH_LO, self.THRESH_HI)
        img_mod = cv2.bitwise_and(img, img, mask=mask)
        img_mod = cv2.cvtColor(img_mod, cv2.COLOR_BGR2GRAY)
        img_mod = cv2.threshold(img_mod, 1, 255, cv2.THRESH_BINARY)[1]
        cv2.line(img_mod, (0, img_mod.shape[0]), (img_mod.shape[1],
                 img_mod.shape[0]), (0, 0, 0), 20)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        img_mod = cv2.erode(img_mod, kernel, iterations=6)
        img_mod = cv2.dilate(img_mod, kernel, iterations=6)
        img_mod = cv2.Canny(img_mod, 30, 150)
        canny = img_mod.copy()

        contours, hierarchy = cv2.findContours(img_mod, cv2.RETR_EXTERNAL,
                                               cv2.CHAIN_APPROX_SIMPLE)
        filtered_conts = []
        for cont in contours:
            arc_len = cv2.arcLength(cont, True)
            approx = cv2.approxPolyDP(cont, 0.05 * arc_len, True)
            area = cv2.contourArea(approx)
            if area > arc_len and area >= 300 and area <= 2000:
                _, _, w, h = cv2.boundingRect(approx)
                ratio = h/w
                if len(approx) == 4 and ratio > 1.5 and ratio < 5:
                    filtered_conts.append(approx)
        #cv2.drawContours(frame, filtered_conts, -1, (0, 0, 255), 5)

        if len(filtered_conts) == 0:
            return True

        sorted_conts = sorted(filtered_conts, key=cv2.contourArea, reverse=True)
        res_cont = sorted_conts[0]

        # TODO: for testing only
        #if len(filtered_conts) > 1:
        #    # TODO: get the one with the largest area
        #    logger.error("More than one contour detected!")
        #    logger.debug(f"# of contours: {len(filtered_conts)}")
        #    for i, x in enumerate(filtered_conts):
        #        logger.debug(f"Contour #: {i}")
        #        _, _, w, h = cv2.boundingRect(x)
        #        ratio = h/w
        #        area = cv2.contourArea(x)

        #        area_res = f"Area: {area}, FAIL" if area < 300 or area > 2000 \
        #                   else f"Area: {area}, PASS"
        #        edge_res = f"Edge: {len(x)}, FAIL" if len(x) != 4 else \
        #                   f"Edge: {len(x)}, PASS"
        #        ratio_res = f"Ratio: {ratio}, FAIL" if ratio <= 1.5 or \
        #                    ratio >= 5 else f"Ratio: {ratio}, PASS"

        #        logger.debug(area_res)
        #        logger.debug(edge_res)
        #        logger.debug(ratio_res)
        #    cv2.drawContours(frame, filtered_conts, -1, (0, 0, 255), 5)
        #    cv2.imshow("more than one", frame)
        #    cv2.waitKey(0)
        #    #exit(1)
        #elif len(filtered_conts) == 0:
        #    logger.error("No contour detected!")
        #    cv2.imshow("color", color)
        #    cv2.imshow("canny", canny)
        #    cv2.waitKey(0)
        #    return False
        #    #exit(1)

        x, y, w, h = cv2.boundingRect(res_cont)
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 3)
        return False

