import glob
import logging
import os
import threading
import time
from queue import Queue

import cv2

from lib.frame import Frame

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

class Cap():
    """
    A parent class representing media feed capture on each pump bay

    Methods
    -------
    Methods of this class must be overwritten by the child class
    start()
        Trigger media feed start capture event
    stop()
        Trigger media feed stop capture event
    get_newest_frame()
        Get the latest frame available in real-time
    """

    def __init__(self, cap_type, cap_source):
        """
        Parameters
        ----------
        cap_type : str
            Capture type. Supported capture type is "jpg", "video" and "rtsp"
        cap_source : str
            Capture source. For "jpg", it must be a folder containing all the
            images. For "video", it must be the path to a local video and for
            "rtsp", the address of the IP camera
        """

        self.cap_type = cap_type
        self.source = cap_source

    def start(self):
        raise NotImplementedError("Method must be overwritten by child class")

    def stop(self):
        raise NotImplementedError("Method must be overwritten by child class")

    def get_newest_frame(self):
        raise NotImplementedError("Method must be overwritten by child class")

class RTSPCap(Cap):
    """A child class of Cap for media capture from an RTSP source"""

    def __init__(self, cap_type, cap_source):
        # TODO: check cap_type and cap_source are valid
        raise NotImplementedError

class VideoCap(Cap):
    """A child class of Cap for media capture from a local video source"""

    def __init__(self, cap_type, cap_source):
        """
        Parameters
        ----------
        frames : Queue
            In-memory buffer containing frames read from a local video source
        cap : cv2.VideoCapture
            OpenCV VideoCapture object for a local video source
        end_cap: threading.Event
            A flag to indicates the end of media capture
        read_t : threading.Thread
            A thread to read frames and populate queue indefinitely
        """

        if not os.path.exists(cap_source):
            raise FileNotFoundError(f"Video file {cap_source} does not exist")
        if cap_type != "video":
            raise NotSuppotedError(f"{cap_type} is not a supported cap type")

        Cap.__init__(self, cap_type, cap_source)
        self.frames = Queue()
        self.cap = cv2.VideoCapture(cap_source)
        self.end_cap = threading.Event()
        self.read_t = None

    def start(self, sample_rate=-1, timeout=10):
        """Trigger video capture start event
:
        Start video capture reading thread and wait until the first frame is
        obtainable. Timeout if the process takes longer than the timeout
        parameter

        Parameters
        ----------
        sample_rate : int, optional
            Number of seconds to sample one frame (default is -1 which is
            getting all available frames)
        timeout : int, optional
            Timeout duration (default is 10 seconds)
        """

        logger.info(f"Starting video capture from {self.source}")
        self.read_t = threading.Thread(target=read_from_video,
                                       args=(self.cap, self.frames,
                                             self.end_cap, sample_rate),
                                       daemon=True)
        self.read_t.start()

        # Wait until we get our first frame or timeout
        start_t = time.time()
        ret = False
        elapsed_s = 0
        logger.info(f"Getting first frame. Please wait...")
        while ret == False and elapsed_s < timeout:
            ret, frame = self.get_newest_frame()
            elapsed_s = int(time.time() - start_t)

        if elapsed_s > timeout:
            self.stop()
            logger.error("Timeout getting frame")
            raise TimeoutError

        logger.info("Video capture is ready")

    def stop(self):
        """Trigger video capture start event"""

        logger.info(f"Stopping video capture for {self.source}. Please wait...")
        self.end_cap.set()
        time.sleep(0.5)
        if self.read_t != None:
            self.read_t.join()
        self.cap.release()

    def get_newest_frame(self):
        """Get the latest frame available in real-time

        Returns
        -------
        bool, (numpy.Array, str)
            Return True if frame is obtainable
        """

        if self.frames.empty():
            return False, (None, "")
        else:
            return True, (self.frames.get(timeout=1))

    def is_running(self):
        """Poll to check if video capture is still running"""

        return not self.end_cap.is_set()

def read_from_video(cap, q, end_cap, sample_rate):
    """A thread to read decoded frames from video capture source

    Run indifinitely until end flag is triggered. Frames from video source
    is pushed to queue based on the sample rate

    Parameters
    ----------
    cap : cv2.VideoCapture
        OpenCV VideoCapture object for a local video source
    q : Queue
        In-memory buffer containing frames read from a local video source to be
        return to the main thread
    end_cap: threading.Event
        A flag to indicates the end of media capture
    sample_rate : int
        Number of seconds to sample one frame
    """

    start_t = time.time()
    first_time = True
    while not end_cap.is_set():
        ret, frame = cap.read()
        if ret == False:
            end_cap.set()
        pos = cap.get(cv2.CAP_PROP_POS_FRAMES)
        # TODO: double check this
        ts = cap.get(cv2.CAP_PROP_POS_MSEC)
        frame_obj = Frame(frame, pos, ts)

        if first_time == True:
            if not q.full():
                q.put(frame_obj, timeout=1)
                first_time = False
        else:
            elapsed_s = time.time() - start_t
            if elapsed_s >= sample_rate:
                if not q.full():
                    q.put(frame_obj, timeout=1)
                start_t = time.time()

class ImageCap(Cap):
    """A child class of Cap for media capture from local images source"""

    def __init__(self, cap_type, cap_source):
        """
        Parameters
        ----------
        frames : Queue
            In-memory buffer containing frames read from a local video source
        """

        # TODO: check cap_type and cap_source are valid
        Cap.__init__(self, cap_type, cap_source)
        self.frames = Queue()

    def start(self):
        """Start reading images from source directory"""

        for filename in glob.iglob(self.source + "/*." + self.cap_type):
            frame = cv2.imread(filename)
            self.frames.put((frame, filename))

    def get_newest_frame(self):
        """Stop reading images"""

        if not self.frames.empty():
            return True, self.frames.get()
        else:
            return False, None

