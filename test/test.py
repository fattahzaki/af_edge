import csv
import json
import os
import sys

class Test():
    def __init__(self, logfile, truth_file):
        if not os.path.exists(logfile) or not os.path.exists(truth_file):
            raise FileNotFoundError
        self.logfile = logfile
        with open(truth_file, "r") as f:
            self.ground_truth = json.load(f)

    def run_test_breakaway_detection(self):
        breakaway_incorrects = {}

        with open(self.logfile, "r") as f:
            res = csv.reader(f)
            for element in res:
                event = element[0]
                if event == "breakaway_decoupling":
                    breakaway_type = element[1]
                    frame_num = int(element[2])
                    if frame_num < self.ground_truth[breakaway_type][0] or \
                            frame_num > self.ground_truth[breakaway_type][1]:
                                if breakaway_incorrects.get(breakaway_type) == None:
                                    breakaway_incorrects[breakaway_type] = []
                                breakaway_incorrects[breakaway_type].append(frame_num)

        for key, val in breakaway_incorrects.items():
            print(key, val)

        if len(breakaway_incorrects) > 0:
            print("Breakaway detection test result: FAILED")
            return 1
        print("Breakaway detection test result: PASSED")
        return 0

    def run_test_sign_detection(self):
        sign_incorrects = []

        with open(self.logfile, "r") as f:
            res = csv.reader(f)
            for element in res:
                event = element[0]
                if event == "sign_not_detected":
                    frame_num = int(element[1])
                    sign_incorrects.append(frame_num)

        for frame in sign_incorrects:
            print(frame)

        if len(sign_incorrects) > 0:
            print("Sign detection test result: FAILED")
            return 1
        print("Sign detection test result: PASSED")
        return 0

