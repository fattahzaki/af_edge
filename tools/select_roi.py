import cv2
import sys

img_filename = sys.argv[1]
img = cv2.imread(img_filename)
roi = cv2.selectROI(img)

print(roi)
cropped = img[int(roi[1]):int(roi[1]+roi[3]), int(roi[0]):int(roi[0]+roi[2])]
cv2.imshow("cropped", cropped)
cv2.waitKey(0)
cv2.imwrite(img_filename + "_cropped.jpg", cropped)
